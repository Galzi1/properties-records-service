const sqlite3 = require("sqlite3");

sqlite3.Database.prototype.runAsync = function (sql, ...params) {
  return new Promise((resolve, reject) => {
    this.run(sql, params, function (err) {
      if (err) return reject(err);
      resolve(this);
    });
  });
};

sqlite3.Database.prototype.runBatchAsync = function (statements) {
  var results = [];
  var batch = ["BEGIN", ...statements, "COMMIT"];
  return batch
    .reduce(
      (chain, statement) =>
        chain.then((result) => {
          results.push(result);
          return db.runAsync(...[].concat(statement));
        }),
      Promise.resolve()
    )
    .catch((err) =>
      db
        .runAsync("ROLLBACK")
        .then(() => Promise.reject(err + " in statement #" + results.length))
    )
    .then(() => results.slice(2));
};

const db = new sqlite3.Database(
  process.env.DB_PATH || "./db/propertiesRecords.db",
  sqlite3.OPEN_READWRITE,
  (err) => {
    if (err) {
      console.error(err.message);
    }
    console.log("Connected to the properties records database.");
  }
);

process.on("SIGTERM", shutdown);
process.on("SIGINT", shutdown);

function shutdown() {
  console.log("Closing database...");
  db.close();
  process.exit(0);
}

module.exports = db;
