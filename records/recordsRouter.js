var express = require("express");
var router = express.Router();

async function getRecords(req, res) {
  try {
    const results = await req.service.getRecords(req.query);
    res.json(results);
  } catch (err) {
    console.error(err);
    res.status(500).send({ error: "Failed due to Server error!" });
  }
}

async function createRecord(req, res) {
  try {
    const results = await req.service.createRecord(req.body);
    res.json(results);
  } catch (err) {
    console.error(err);
    res.status(500).send({ error: "Failed due to Server error!" });
  }
}

router.get("/", getRecords);
router.post("/", createRecord);

module.exports = router;
