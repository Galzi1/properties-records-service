const { parseDate } = require("../utils");

function getDateFilterClause(fromDate, toDate) {
  if (!fromDate || !toDate) {
    return "";
  }
  const fromDateObject = parseDate(fromDate);
  const [fromDay, fromMonth, fromYear] = [
    fromDateObject.date(),
    fromDateObject.month() + 1,
    fromDateObject.year(),
  ];
  const toDateObject = parseDate(toDate);
  const [toDay, toMonth, toYear] = [
    toDateObject.date(),
    toDateObject.month() + 1,
    toDateObject.year(),
  ];

  const fromDateClause = `(DateYear = ${fromYear} AND DateMonth = ${fromMonth} AND DateDay >= ${fromDay}) OR (DateYear = ${fromYear} AND DateMonth > ${fromMonth}) OR DateYear > ${fromYear}`;
  const toDateClause = `(DateYear = ${toYear} AND DateMonth = ${toMonth} AND DateDay < ${toDay}) OR (DateYear = ${toYear} AND DateMonth < ${toMonth}) OR DateYear < ${toYear}`;
  return `(${fromDateClause}) AND (${toDateClause})`;
}

class RecordsService {
  constructor(db) {
    this.db = db;
  }
  //sortBy=email&orderBy=asc
  async getRecords(query) {
    const propertyId = query.propertyId;
    const propertyFilterClause = propertyId ? "PropertyID=" + propertyId : "";

    const fromDate = query.fromDate;
    const toDate = query.toDate;
    const dateFilterClause = getDateFilterClause(fromDate, toDate);

    let filterClause;
    if (propertyFilterClause) {
      filterClause = `WHERE ${propertyFilterClause}`;
      if (dateFilterClause) {
        filterClause += ` AND ${dateFilterClause}`;
      }
    } else if (dateFilterClause) {
      filterClause = `WHERE ${dateFilterClause}`;
    }

    const sortBy = query.sortBy;
    const orderBy = query.orderBy;
    const dateSortingClause =
      sortBy === "date" && (orderBy === "asc" || orderBy === "desc")
        ? `ORDER BY DateYear ${orderBy.toUpperCase()}, DateMonth ${orderBy.toUpperCase()}, DateDay ${orderBy.toUpperCase()}`
        : "";

    const sql = `SELECT * FROM Record ${filterClause} ${dateSortingClause}`;
    return new Promise((resolve, reject) => {
      this.db.all(sql, [], function cb(err, rows) {
        if (err) {
          reject(err);
        } else {
          resolve(rows);
        }
      });
    });
  }

  async createRecord(record) {
    const addRecordSql = `INSERT INTO Record(PropertyID,Amount,DateDay,DateMonth,DateYear) VALUES(?,?,?,?,?)`;
    const updateMonthlyBalanceSql = `INSERT INTO MonthlyBalance(PropertyID,Month,Year,Balance) VALUES(?,?,?,?) ON CONFLICT(PropertyID,Month,Year) DO UPDATE SET Balance = Balance + excluded.Balance;`;
    return new Promise((resolve, reject) => {
      let responseObj;
      const dateObject = parseDate(record.date);
      if (!dateObject) {
        reject("Error parsing date " + record.date);
      }
      const valuesToInsert = [
        record.propertyID,
        record.amount,
        dateObject.date(),
        dateObject.month() + 1,
        dateObject.year(),
      ];

      const statements = [
        [addRecordSql, ...valuesToInsert],
        [
          updateMonthlyBalanceSql,
          record.propertyID,
          dateObject.month() + 1,
          dateObject.year(),
          record.amount,
        ],
      ];
      this.db
        .runBatchAsync(statements)
        .then((results) => {
          responseObj = {
            statement: this,
            rows: results,
          };
          resolve(responseObj);
        })
        .catch((err) => {
          responseObj = {
            error: err,
          };
          reject(responseObj);
        });
    });
  }
}

module.exports = RecordsService;
