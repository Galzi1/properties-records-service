const express = require("express");
const logger = require("morgan");

const db = require("./db/db");

const app = express();

app.use(logger(process.env.ENVIRONMENT || "dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

const RecordsService = require("./records/recordsService");
const recordsServiceInstance = new RecordsService(db);

const exposeRecordsService = async (req, res, next) => {
  req.service = recordsServiceInstance;
  next();
};

const recordsRouter = require("./records/recordsRouter");
app.use("/records", exposeRecordsService, recordsRouter);

const MonthlyBalancesService = require("./monthlyBalances/monthlyBalancesService");

const exposeMonthlyBalancesService = async (req, res, next) => {
  req.service = new MonthlyBalancesService(db, recordsServiceInstance);
  next();
};

const monthlyBalancesRouter = require("./monthlyBalances/monthlyBalancesRouter");
app.use(
  "/monthlyBalanceReports",
  exposeMonthlyBalancesService,
  monthlyBalancesRouter
);

// error handler
app.use(function (err, req, res, next) {
  if (req.app.get("env")) {
    console.log(err);
  }

  res.status(err.status || 500);
  res.json(err.message || "An error occurred");
});

module.exports = app;
