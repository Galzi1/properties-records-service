# properties-records-service

Gal Ziv

## Requirements

### The microservice shall allow the user to do the following:

#### Add a new record with at least the following information:

- Property ID.
- Amount.
- Date.

#### Get a monthly balance report.

The balance report shall contain the starting balance and all the
records in the selected month, with the balance next to each line. For example:
Starting cash = $200
Record 1=> type=income, amount=$100, $300
Record 2=> type=expense, amount=$250, $50
Ending cash = $50
The monthly report is required to be available for the trailing 12 months.
(You can support more, but it's not required)

#### Get all records of a property. The user should be able to:

- Filters by type (expense/income) and/or dates(from/to).
- Sort by dates (ascending or descending)
- Pagination.

#### Get the current balance of a property.

### Additional Information and Notes

- We expect to have ten times more reads than writes.
- A property could have up to 1M records per property per year.
- AuthN/Z is not required.
- You must write tests only for the most critical area of your code. Any other tests are optional.
- You can add more functionally if needed.
- You can use any technology / 3rd party tools / components you want.
- You should prefer a database to store the data.
- Prioritize a working solution over everything else.
- Limit your work to 6 hours.

### Evaluation

We will mainly take into consideration the following things:

- Does your code implement the business rules correctly?
- How did you decide to organize and structure the application?
- How well does it perform?
- Will it be maintainable by other developers?
- Where and how do you handle edge cases?
- Is the code have a consistent coding style?
- How do you use Git during your development process?

## Edge cases

- The first record (by date) is an expense (will make the balance negative).
- Adding two records with dates being in a certain month, and then adding a new record in between them, changing the month's balance.
- User requests a monthly balance report for January of a certain year (the starting cash value should be taken from a month in the previous year).
- User tries to add a record with Date in the future (we will not allow it).
- Integer overflow (either positive or negative integer).

## How to run

- For running the service run node at the service's root directory ('node .'). You can use 'npm run start' command.
- For running the tests run jest at the service's root directory ('jest'). You can use 'npm run test' command.

## Trade-offs and additional possible improvements

- Because we know that we will have 10x more reads than writes, and because of the scale of the data, it would be more efficient to cache the monthly balance report requests after each request, and evict the cache entry upon adding a record on the same month or before. I did not implement it due to time limitations.
- I used sqlite database for ease of implementations reasons, optimally I would use a SQL database better fitting large scale data (like MySQL, PostgreSQL, Oracle etc.).

## API Documentation

- Get All Records -> HTTP GET /records
  - Property Filtering (should be mandatory in production to prevent data leakage between properties of different customers) -> Query param 'propertyId' ; Example path: /records?propertyId=1
  - Date Filtering -> Query params 'fromDate' and 'toDate' ; Example path: /records?fromDate='15/02/2023'&toDate='18/04/2023'
  - Sorting by Date -> Query params 'soryBy' and 'orderBy' (for date filtering 'sortBy' = date. 'orderBy' = 'asc' | 'desc') ; Example path: /records?sortBy=date&orderBy=asc
- Add New Record -> HTTP POST /records
  - Example body payload (JSON): {
    "propertyID" : 1,
    "amount" : 40,
    "date" : "10/03/2023"
    }
- Get Monthly Balance Report -> HTTP GET /monthlyBalanceReports
  - Mandatory filtering query params 'propertyId', 'month' and 'year' ; Example path: /monthlyBalanceReports?propertyId=1&month=3&year=2023
