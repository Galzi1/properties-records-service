const moment = require("moment");

function parseDate(dateStr) {
  if (!dateStr) {
    return null;
  }
  try {
    return moment(dateStr, "DD-MM-YYYY");
  } catch (err) {
    return null;
  }
}

function getMonthStart(month, year) {
  return `01/${month}/${year}`;
}

function getNextMonthStart(month, year) {
  if (month === 12) {
    return `01/01/${year + 1}`;
  }
  return getMonthStart(month + 1, year);
}

module.exports = { parseDate, getMonthStart, getNextMonthStart };
