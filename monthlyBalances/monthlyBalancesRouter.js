var express = require("express");
var router = express.Router();

async function getMonthlyBalanceReport(req, res) {
  try {
    const results = await req.service.getMonthlyBalanceReport(req.query);
    res.json(results);
  } catch (err) {
    console.error(err);
    res.status(500).send({ error: "Failed due to Server error!" });
  }
}

router.get("/", getMonthlyBalanceReport);

module.exports = router;
