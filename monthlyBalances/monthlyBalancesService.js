const { getMonthStart, getNextMonthStart } = require("../utils");

class MonthlyBalancesService {
  constructor(db, recordsService) {
    this.db = db;
    this.recordsService = recordsService;
  }

  async getMonthlyBalanceReport(query) {
    return new Promise(async (resolve, reject) => {
      try {
        const propertyId = query.propertyId;
        const month = +query.month;
        const year = +query.year;
        const monthlyBalancesUpToDate = await this.getPastMonthlyBalances(
          propertyId,
          month,
          year
        );
        const totalMonthlyBalanceUpToDate = monthlyBalancesUpToDate.reduce(
          (sum, value) => sum + value.BALANCE,
          0
        );
        const startingCashLog =
          "Starting cash = $" + totalMonthlyBalanceUpToDate;

        const { monthRecordsLogs, rollingBalance } =
          await this.getMonthlyRecordsLogsAndBalance(
            propertyId,
            month,
            year,
            totalMonthlyBalanceUpToDate
          );

        const endingCashLog = "Ending cash = $" + rollingBalance;
        resolve(
          [startingCashLog, ...monthRecordsLogs, endingCashLog].join("\n")
        );
      } catch (err) {
        reject(err);
      }
    });
  }

  async getMonthlyRecordsLogsAndBalance(
    propertyId,
    month,
    year,
    startingBalance
  ) {
    const monthRecords = await this.recordsService.getRecords({
      propertyId: propertyId,
      fromDate: getMonthStart(month, year),
      toDate: getNextMonthStart(month, year),
      sortBy: "date",
      orderBy: "asc",
    });

    let rollingBalance = startingBalance;
    const monthRecordsLogs = monthRecords.map((record, index) => {
      rollingBalance += record.Amount;
      return `Record ${index + 1} => type=${
        record.Amount >= 0 ? "income" : "expense"
      }, amount=$${Math.abs(record.Amount)}, $${rollingBalance}`;
    });

    return { monthRecordsLogs, rollingBalance };
  }

  async getMonthlyBalance(propertyId, month, year) {
    const sql = `SELECT Balance From MonthlyBalance WHERE PropertyID = ? AND Month = ? AND year = ?`;
    return new Promise((resolve, reject) => {
      this.db.all(sql, [propertyId, month, year], function cb(err, rows) {
        if (err) {
          reject(err);
        } else {
          resolve(rows);
        }
      });
    });
  }

  async getPastMonthlyBalances(propertyId, month, year) {
    const sql = `SELECT Balance From MonthlyBalance WHERE PropertyID = ? AND (Year < ? OR (Year = ? AND Month < ?))`;
    return new Promise((resolve, reject) => {
      this.db.all(sql, [propertyId, year, year, month], function cb(err, rows) {
        if (err) {
          reject(err);
        } else {
          resolve(rows);
        }
      });
    });
  }
}

module.exports = MonthlyBalancesService;
