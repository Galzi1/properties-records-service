const { expect } = require("chai");
const MonthlyBalancesService = require("./monthlyBalancesService");
describe("MonthlyBalancesService", () => {
  const mockRecordsService = {
    getRecords: (query) => [
      {
        ID: 11,
        PropertyID: 1,
        Amount: 40,
        DateDay: 10,
        DateMonth: 3,
        DateYear: 2023,
      },
      {
        ID: 30,
        PropertyID: 1,
        Amount: -100,
        DateDay: 4,
        DateMonth: 3,
        DateYear: 2023,
      },
    ],
  };
  const monthlyBalancesService = new MonthlyBalancesService(
    undefined,
    mockRecordsService
  );

  describe("type of monthlyBalancesService.getMonthlyRecordsLogsAndBalance()", () => {
    it("should be function", () => {
      expect(monthlyBalancesService.getMonthlyRecordsLogsAndBalance).to.be.a(
        "function"
      );
    });
  });

  describe("output of monthlyBalancesService.getMonthlyRecordsLogsAndBalance()", () => {
    it("should be correct", async () => {
      const { monthRecordsLogs, rollingBalance } =
        await monthlyBalancesService.getMonthlyRecordsLogsAndBalance(
          0,
          0,
          0,
          -40
        );
      expect(rollingBalance).to.equal(-100);
      expect(monthRecordsLogs).to.be.an("array");
      expect(monthRecordsLogs).to.have.lengthOf(2);
      expect(monthRecordsLogs[0]).to.equal(
        "Record 1 => type=income, amount=$40, $0"
      );
      expect(monthRecordsLogs[1]).to.equal(
        "Record 2 => type=expense, amount=$100, $-100"
      );
    });
  });
});
